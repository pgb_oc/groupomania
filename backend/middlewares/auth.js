const jwt = require('jsonwebtoken');

// check userId verified with token
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, process.env.TOKEN_SECRET_KEY);
        //const userId = decodedToken.userId;
        //const admin = decodedToken.admin;

        const data = req.session.cookie;
        const { userId } = req.session
        const { admin } = req.session

        if (req.body.userId && req.body.userId !== userId) {
            return res.status(401).json({ error: "Id de l'utilisateur non valide !" })
        } else if (req.body.admin && req.body.admin !== admin) {
            console.log(admin)
            return res.status(401).json({ error: "Problème de droits pour l'utilisateur !" })
        } else {
            console.log(decodedToken)
            next();
        }
    } catch (error) {
        res.status(401).json({ error: error | 'Requête non authentifiée !' });
    }
};