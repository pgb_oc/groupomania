const multer = require('multer'); // multer module import

const MIME_TYPES = { // define accepted files type
    'image/jpg': 'jpg',
    'image/jpeg': 'jpg',
    'image/png': 'png',
}

const storage = multer.diskStorage({
    destination: (req, file, callback) => { // images stockage path/folder
        callback(null, 'images')
    },
    filename: (req, file, callback) => {
        const name = file.originalname.split(' ').join('_'); // replace random space by underscore
        const extension = MIME_TYPES[file.mimetype]; // file extention generation by MIME_TYPES
        callback(null, name + Date.now() + '.' + extension); // unique image name creation (no clone)
    }
});

module.exports = multer({ storage }).single('image');