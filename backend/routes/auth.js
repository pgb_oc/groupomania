const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation

const authController = require('../controllers/auth'); // user path in controllers

// login/sign/logout routes
router.post('/signup', authController.signup);
router.post('/login', authController.login);
router.get('/logout', authController.logout);

module.exports = router; // router export