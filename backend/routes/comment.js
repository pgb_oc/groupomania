const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const auth = require('../middlewares/auth'); // auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth
const commentController = require('../controllers/comment'); // comment path instructions

// auth middleware protect routes connec by auth token processus

// Create
router.post('/', auth, multer, commentController.createComment);

// Read
router.get('/', auth, commentController.getAllComments);
router.get('/:id', auth, commentController.getOneComment);

// Delete
router.delete('/:id', auth, commentController.deleteComment);


module.exports = router;