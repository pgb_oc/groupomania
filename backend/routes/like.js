const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const auth = require('../middlewares/auth'); // auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth
const likeController = require('../controllers/like');// likes path instructions


// auth middleware protect routes connec by auth token processus

// Create
router.post('/', auth, multer, likeController.createLike);

// Read
router.get('/', auth, likeController.getAllLikes);


module.exports = router;