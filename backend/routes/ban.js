const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const auth = require('../middlewares/auth'); // auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth
const banController = require('../controllers/ban'); // comment path instructions

// auth middleware protect routes connec by auth token processus

// Create
router.post('/', auth, multer, banController.createBan);

module.exports = router;