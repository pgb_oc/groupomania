// imports
const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const postController = require('../controllers/post'); // posts path instructions
const commentController = require('../controllers/comment'); // comments path instructions
const likeController = require('../controllers/like'); // likes path instructions
const auth = require('../middlewares/auth');// auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth

// auth middleware protect routes connec by auth token processus

// Create
router.post('/', auth, multer, postController.createPost);

// Read
router.get('/', auth, postController.getAllPosts);
router.get('/:id/comments', auth, commentController.getAllComments);
router.get('/:id/likes', auth, likeController.getAllLikes);
router.get('/:id', auth, postController.getPost);

// Update
router.put('/:id', auth, multer, postController.updatePost);

// Delete
router.delete('/:id', auth, postController.deletePost);

module.exports = router;