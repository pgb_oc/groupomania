const express = require('express'); // express module import (app infrastructure)
const router = express.Router(); // router creation
const userController = require('../controllers/user'); // users path instructions
const postController = require('../controllers/post');// posts path instructions
const auth = require('../middlewares/auth');// auth path, validation by token
const multer = require('../middlewares/multer-config'); // multer path, multer declared after auth

// auth middleware protect routes connec by auth token processus

// Read
router.get('/', auth, userController.getAllUsers);
router.get('/:id', auth, userController.getOneUser);
router.get('/:id/posts', auth, postController.getAllPostsByUserId);

// Update
router.put('/:id', auth, multer, userController.updateUser);

// Delete
router.delete('/:id', auth, userController.deleteUser);

module.exports = router;