// imports
const cors = require('cors');
const express = require('express'); // express import
const app = express(); // express initialization

const bodyParser = require('body-parser'); // bodyParser import, body JSON request analyser

const path = require('path');
const auth = require('./middlewares/auth');
require('dotenv').config(); // dotenv (load env vars) module import (separe config)
const limiter = require('./middlewares/rate-limits-sec'); // import req limiter
const helmet = require('helmet');// helmet header sec import

// call routes
const authRoutes = require('./routes/auth');
const userRoutes = require('./routes/user');
const postRoutes = require('./routes/post');
const commentRoutes = require('./routes/comment');
const likeRoutes = require('./routes/like');
const banRoutes = require('./routes/ban');

// Sequelize database models
const db = require("./models");
db.sequelize.sync();

// CORS security
app.use(cors({
  origin: "http://localhost:3000",
  credentials: true,
  exposedHeaders: []
  // allowedHeaders: ["Origin", "X-Requested-With", "Content", "Accept", "Content-Type", "Authorization", "Set-Cookie", "Cookies"],
}));
  
app.use(bodyParser.urlencoded({
    extended: true,
    credentials: true,
}));

app.use((req, res, next) => {
  //res.setHeader('Access-Control-Allow-Origin', '*'); // All origins
  res.setHeader('Access-Control-Allow-Headers', ["Origin", "X-Requested-With", "Content", "Accept", "Content-Type", "Authorization", "Set-Cookie", "Cookies"].join(',')); // 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization'); // Allowed header
  //res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS'); // Allowed verbs
  if (req.method === 'OPTIONS') {
    res.sendStatus(200);
  }
  else {
    next();
  }
});

// Define HTTP headers helmet security
app.use(helmet());
// Security: disable X-Powered-By header (auto in helmet module)
//app.disable('x-powered-by');

const mysql = require('mysql');
const session = require('express-session');
 
const mysqlStore = require('express-mysql-session')(session);

// Manage session with mysql
const options ={
  connectionLimit: 10,
  password: process.env.DB_PASS,
  user: process.env.DB_USER,
  database: process.env.DB_NAME,
  host: process.env.DB_HOST,
  port: process.env.DB_PORT,
  createDatabaseTable: true
}

const pool = mysql.createPool(options);
const  sessionStore = new mysqlStore(options, pool);


// active session with mysql
const IN_PROD = process.env.NODE_ENV === 'production'
const TWO_HOURS = 1000 * 60 * 60 * 2

app.use(session({
  name: process.env.SESS_NAME,
  resave: false,
  saveUninitialized: false,
  store: sessionStore,
  secret: process.env.SESS_SECRET,
  cookie: {
      maxAge: TWO_HOURS,
      secure: IN_PROD,
  }
}))
/*
// add logout route for delete session
app.get("/api/logout", (req, res) => {
  req.session.destroy(err => {
    if(err){
      console.log("Error occur during log out", err)
      res.status(400).json({ err })
    }
    sessionStore.close()
    res.clearCookie(process.env.SESS_NAME)
    console.log("Log process endend successfully")
    res.status(200).json({ message: 'Session utilisateur supprimé' })
  })
});
*/
// Global routes paths
app.use(bodyParser.json({ limit: "100kb" })); // parse and limit payload size of requests
//app.use(bodyParser.urlencoded({ extended: true }));

// Global API routes
app.use('/images', express.static(path.join(__dirname, 'images'))); // images storage location, 'images' is now static for express
app.use('/api/auth', limiter, authRoutes);
app.use('/api/users', auth, userRoutes);
app.use('/api/posts', limiter, auth, postRoutes);
app.use('/api/comments', limiter, auth, commentRoutes);
app.use('/api/likes', limiter, auth, likeRoutes);
app.use('/api/bans', limiter, auth, banRoutes);

// Global 404 error
app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

// export de notre app
module.exports = app;