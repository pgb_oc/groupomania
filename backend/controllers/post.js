// imports
const models = require("../models");
const Post = models.posts; // sequilize datas model import
const Comment = models.comments; // sequilize datas model import
const Like = models.likes; // sequilize datas model import
const fs = require('fs'); // native file system manager import

/* ===================================================================================================== */
/* CREATE                                                                                                */
/* ===================================================================================================== */

exports.createPost = (req, res) => {
  // Init request elements
  const title = req.body.title;
  const text = req.body.text;

  if (title === null || title === '' || text === null || text === '') { // check if all fields have values
    return res.status(400).json({ 'error': "Tous les champs sont obligatoires." });
  }

  const post = new Post( req.file ? // New instance of Post model
    {
      ...req.body.post, 
      postUrl: req.file.filename
    } : { ...req.body }, // SPREAD Operator = all el of req.body
  );

  // save in db
  post.save()
    .then(() => res.status(201).json({ message: 'Post créé !' }))
    .catch(error => res.status(400).json({ error }));
}

/* ===================================================================================================== */
/* READ                                                                                              */
/* ===================================================================================================== */

exports.getPost = (req, res) => {
  Post.findOne({ where: { id: req.params.id } }) // method findOne, find the unique Post by id
    .then(post => res.status(200).json(post))
    .catch(error => res.status(404).json({ error }));
  console.log(req.body);
};

exports.getAllPosts = (req, res, next) => {
  Post.findAll({// find method return all posts from DB
    order: [
      ['createdAt', 'DESC'],
    ]
  })
    .then(posts => {
      console.log(posts);
      res.status(200).json({ data: posts });
    })
    .catch(error => res.status(400).json({ error }));
};

exports.getAllPostsByUserId = (req, res, next) => {
  Post.findAll({ // find all posts by user id
    where: { userId: req.params.id },
    order: [
      ['createdAt', 'DESC'],
    ]
  })
    .then(posts => res.status(200).json({ data: posts }))
    .catch(error => res.status(400).json({ error }));
};

/* ===================================================================================================== */
/* UPDATE                                                                                            */
/* ===================================================================================================== */

exports.updatePost = (req, res, next) => { // post modif
  // Init request elements
  const title = req.body.title;
  const text = req.body.text;

  if (title === null || title === '' || text === null || text === '') { // check if fields have values
    return res.status(400).json({ 'error': "Tous les champs sont obligatoires." });
  }

  // update post img
  const postObject = req.file ?
    {
      ...req.body.user,
      postUrl: req.file.filename
    } : { ...req.body };

  // update selected post
  Post.update({ ...postObject, id: req.params.id }, { where: { id: req.params.id } }) // method update, Update with the same id as param

    .then(() => res.status(200).json({ message: 'Post modifié !' }))
    .catch(error => res.status(400).json({ error }));
};

/* ===================================================================================================== */
/* DELETE                                                                                            */
/* ===================================================================================================== */

exports.deletePost = (req, res, next) => {
  Like.destroy({ where: { postId: req.params.id } }) // find post by id in db 
    .then(() =>
      Comment.destroy({ where: { postId: req.params.id } }) // delete comments
        .then(() =>
          Post.destroy({ where: { id: req.params.id } }) // delete post
        )
    )
    .then(() => res.status(200).json({ message: 'Post supprimé !' }))
    .catch(error => res.status(400).json({ error }));
};