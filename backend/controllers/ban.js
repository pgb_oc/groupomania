const db = require("../models");
const Comment = db.comments;
const Bans = db.ban;
const Posts = db.posts;

exports.createBan = async (req, res) => {
    // Init request elements
    const postId = req.body.postId;
    let userId = req.body.userId;
    const bannedUserId = req.body.bannedUserId;
  
    if (postId && !userId) {
        userId = (await Posts.findByPk(postId)).userId; 
    }

    if (userId === null || userId === '' || bannedUserId === null || bannedUserId === '') { // check if all fields have values
      return res.status(400).json({ 'error': "Tous les champs sont obligatoires." });
    }
  
    const ban = new Bans({
        userId,
        bannedUserId,
    });
  
    if (userId == bannedUserId) {
        return res.status(400).json({ 'error': "Vous pouvez pas vous bannir vous-même." });
    }

    // save in db
    ban.save()
      .then(() => {
          return Posts.findAll({
              where: {
                userId,
              }
          })
          .then(posts => {
              if (!posts) {
                  return [];
              }

              let comments = [];
              for (let post of posts) {
                  comments.push(Comment.findAll({
                      where: {
                        postId: post.id,
                        userId: ban.bannedUserId,
                      } 
                  }));
              }

              return Promise.all(comments).then(arr => arr.flat());
          })
          .then(comments => {
              const destroy = [];
              for (let comment of comments) {
                destroy.push(Comment.destroy({ where: { id: comment.id } }));
              }
              return Promise.all(destroy);
          })
          .then(() => res.status(201).json({ message: 'Ban créé !' }))
      })
      .catch(error => res.status(400).json({ error }));
}