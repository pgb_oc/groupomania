// imports
const models = require("../models");
const User = models.users;
const Post = models.posts;
const Comment = models.comments;
const Like = models.likes;
const fs = require('fs'); // native file system manager import

exports.getAllUsers = (req, res, next) => {
  User.findAll()
    .then(users => {
      console.log(users);
      res.status(200).json({ data: users });
    })
    .catch(error => res.status(400).json({ error }));
};

exports.getOneUser = (req, res, next) => {
  User.findOne({ where: { id: req.params.id } })
    .then(user => {
      res.status(200).json(user)
    })
    .catch(error => res.status(404).json({ error }));
};

exports.getAllUserByName = (req, res, next) => {

  User.findAll({ where: { firstname: req.params.name } })
    .then(users => {
      res.status(200).json(users)
    })
    .catch(error => res.status(404).json({ error }));
};


exports.updateUser = (req, res, next) => {
  // Init request elements
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;

  // check if all fields have values
  if (firstname === null || firstname === '' || lastname === null || lastname === '') {
    return res.status(400).json({ 'error': "Tous les champs sont obligatoires." });
  }

  // update user profile img
  const userObject = req.file ?
    {
      ...req.body.user,
      imageUrl: req.file.filename
    } : { ...req.body };

  User.update({ ...userObject, id: req.params.id }, { where: { id: req.params.id } })
    .then(() => res.status(200).json({ message: 'Utilisateur modifié !' }))
    .catch(error => res.status(400).json({ error }));
};

exports.deleteUser = (req, res, next) => {
  Like.destroy({ where: { userId: req.params.id } })
    .then(() =>
      Comment.destroy({ where: { userId: req.params.id } })
        .then(() =>
          Post.findAll({ where: { userId: req.params.id } })
            .then(
              (posts) => {
                posts.forEach(
                  (post) => {
                    Comment.destroy({ where: { postId: post.id } })
                    Like.destroy({ where: { postId: post.id } })
                    Post.destroy({ where: { id: post.id } })
                  }
                )
              }
            )
            .then(() =>
              User.findOne({ where: { id: req.params.id } })
                .then(user => {
                  const filename = user.imageUrl;
                  fs.unlink(`images/${filename}`, () => { // delete file with function unlink (fs module) + execute callback
                    User.destroy({ where: { id: req.params.id } })
                      .then(() => res.status(200).json({ message: 'Utilisateur supprimé !' }))
                  })
                })
            )
        )
    )
    .catch(error => res.status(400).json({ error }));
};