const db = require("../models");
const Comment = db.comments;
const Bans = db.ban;

exports.getAllComments = (req, res, next) => {
  Comment.findAll({ where: { postId: req.params.id } })
    .then(comments => {
      console.log(comments);
      res.status(200).json({ data: comments });
    })
    .catch(error => res.status(400).json({ error }));
};

exports.getOneComment = (req, res, next) => {
  Comment.findOne({ where: { id: req.params.id } })
    .then(comment => {
      console.log(comment);
      res.status(200).json(comment)
    })
    .catch(error => res.status(404).json({ error }));
};

exports.createComment = (req, res, next) => {
  const commentObject = req.body;
  const comment = new Comment({ // New comment object
    ...commentObject
  });
  Bans.isBannedOnPost({id: req.body.userId}, {id: req.body.postId})
    .then(isBanned => {
      if (isBanned) {
        res.status(400).send("You are banned by this user.");
      } else {
        comment.save() // Save in db

          .then(() => {
            Comment.findAll({
              where: { postId: req.body.postId }
            })
              .then((comments) => {
                res.status(200).json(comments);
              })
          })
          .catch(error => res.status(400).json({ error }));
      }
    })
    .catch(err => {
      res.status(400).json({ err })
    })
}

exports.deleteComment = (req, res, next) => {
  Comment.destroy({ where: { id: req.params.id } })
    .then(() => res.status(200).json({ message: 'Commentaire supprimé !' }))
    .catch(error => res.status(400).json({ error }));
};