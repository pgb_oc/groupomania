// imports
const db = require("../models");
const User = db.users;

const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const mysql = require('mysql');
const session = require('express-session');
 // Manage session with mysql
const options ={
    connectionLimit: 10,
    password: process.env.DB_PASS,
    user: process.env.DB_USER,
    database: process.env.DB_NAME,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    createDatabaseTable: true
  }
const mysqlStore = require('express-mysql-session')(session);
const pool = mysql.createPool(options);
const  sessionStore = new mysqlStore(options, pool);



const maxAge = 3 * 24 * 60 * 60 * 1000;

// New user creation
exports.signup = (req, res, next) => {
    // Init request elements
    const firstname = req.body.firstname;
    const lastname = req.body.lastname;
    const email = req.body.email;
    const password = req.body.password;

    // check if fields have values
    if (firstname === null || firstname === '' || lastname === null || lastname === ''
        || email === null || email === '' || password === null || password === '') {
        return res.status(400).send("Tous les champs sont obligatoires.");
    }

    // Hide email
    let buff = new Buffer(email);
    let emailInbase64 = buff.toString('base64');

    User.findOne({ // check in db if user exist
        attributes: ['email'],
        where: { email: emailInbase64 }
    })
        .then((userCheck) => { // check if user data's exist
            if (!userCheck) { // if not exist
                // Hash du mot de passe avec bcrypt
                bcrypt.hash(password, 10) // Hash password x10 with bcrypt
                    .then(hash => {
                        let buff = new Buffer(email); // Hide email
                        let emailInbase64 = buff.toString('base64');

                        const user = new User({ // New user creation
                            firstname: firstname,
                            lastname: lastname,
                            email: emailInbase64,
                            password: hash
                        })
                        
                        user.save() // Save in db
                            .then(() => {
                                req.session.userId = user.id,
                                req.session.admin = user.admin,
                                res.status(201).json({ message: 'Utilisateur créé !' });
                            })
                            .catch(error => res.status(400).json({ error }));
                    })
            } else if (userCheck) {
                return res.status(409).send("test");//.json(toast.error("L'utilisateur existe déjà !"));
            }
        })
        .catch(error => res.status(500).json({ error }));
};

// Existing user login
exports.login = (req, res, next) => {
    let buff = new Buffer(req.body.email); // Hide email
    let emailInbase64 = buff.toString('base64');

    // Check if user in db
    User.findOne({ where: { email: emailInbase64 } })
        .then(user => {
            if (!user) { // if no user found
                return res.status(401).send("Utilisateur introuvable !");
            }
            bcrypt.compare(req.body.password, user.password) // compare password request with hash saved in db
                .then(valid => {
                    if (!valid) {
                        return res.status(401).send("Mot de passe non valide !");
                    }
                    req.session.userId = user.id;
                    req.session.admin = user.admin;
                    res.status(200).json({
                        userId: user.id,
                        userAdmin: user.admin,
                        token: jwt.sign( // user token creation
                            {
                                userId: user.id,
                                admin: user.admin
                            },
                            process.env.TOKEN_SECRET_KEY,
                            { expiresIn: maxAge }
                        )
                    });
                })
                .catch(error => res.status(500).json({ error }));
        })
        .catch(error => res.status(500).json({ error }));
};

// Logout
exports.logout = (req, res) => {
    req.session.destroy(err => {
        if(err){
          console.log("Error occur during log out", err)
          res.status(400).json({ err })
        }
        res.clearCookie("jwt");
        sessionStore.close()
        res.clearCookie(process.env.SESS_NAME)
        console.log("Log process endend successfully")
        res.status(200).json({ message: 'Session utilisateur supprimé' })
      })
};