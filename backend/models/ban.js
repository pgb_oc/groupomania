'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes, db) => {
  class Ban extends Model {
    static associate(models) {
      models.Ban.hasMany(models.User, {
        foreignKey: 'id',
        sourceKey: 'userId'
      });
      models.Ban.hasMany(models.User, {
        foreignKey: 'id',
        sourceKey: 'bannedUserId'
      });
    }

    static isBannedByUser(maybeBannedUser, targetUser) {
        return Ban.findOne({
            where: {
                userId: targetUser.id,
                bannedUserId: maybeBannedUser.id,
            }
        }).then(ban => {
            return ban != null;
        });
    }

    static isBannedOnPost(maybeBannedUser, targetPost) {
        const posts = db.posts;
        const users = db.users;
        return posts.findByPk(targetPost.id)
            .then(post => {
                if (!post) {
                    return null;
                }

                return users.findByPk(post.userId);
            })
            .then(user => {
                if (!user) {
                    return false;
                }

                return Ban.isBannedByUser(maybeBannedUser, user);
            })
    }
  };
  Ban.init({
    userId: DataTypes.INTEGER,
    bannedUserId: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Ban',
    indexes: [{
        unique: true,
        fields: ['userId', 'bannedUserId'],
    }]
  });
  return Ban;
};