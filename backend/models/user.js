'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate(models) {
      models.User.hasMany(models.Post,
        { onDelete: 'cascade' });

      models.User.hasMany(models.Comment,
        { onDelete: 'cascade' });

      models.User.hasMany(models.Like,
        { onDelete: 'cascade' });
    }
  };
  User.init({
    firstname: DataTypes.STRING,
    lastname: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING,
    desc: DataTypes.STRING,
    imageUrl: {
      type: DataTypes.STRING,
      defaultValue: "default-user.png"
    },
    admin: DataTypes.BOOLEAN
  }, {
    sequelize,
    modelName: 'User',
  });
  return User;
};