'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Post extends Model {
    static associate(models) {
      models.Post.belongsTo(models.User, {
        foreignKey: {
          allowNull: false
        }
      });
      models.Post.hasMany(models.Comment,
        { onDelete: 'cascade' });

      models.Post.hasMany(models.Like,
        { onDelete: 'cascade' });
    }
  };
  Post.init({
    userId: DataTypes.INTEGER,
    title: DataTypes.STRING,
    text: DataTypes.TEXT,
    postUrl: {
      type: DataTypes.STRING,
      defaultValue: "default-post.png"
    },
  },
    {
      sequelize,
      modelName: 'Post',
    });
  return Post;
};

