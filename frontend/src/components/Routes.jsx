import React from 'react'; // Import react
import { Route, Switch, Redirect } from 'react-router-dom'; // Import router

// components
import AuthApi from './AuthApi';
import Home from './Home';
import Signup from './Auth/Signup';
import Login from './Auth/Login';
import Logout from './Auth/Logout';
import User from './Users/User';
import UsersPage from './Users/UsersPage';
import UpdateAccount from './Users/UpdateAccount';
import DeleteAccount from './Users/DeleteAccount';
import DeleteUserAccount from './Users/DeleteUserAccount';
import Posts from './Posts/Posts';
import PostPage from './Posts/PostPage';
import CreatePost from './Posts/CreatePost';
import UpdatePost from './Posts/UpdatePost';
import DeletePost from './Posts/DeletePost';
import DeleteComment from './Comments/DeleteComment';
import UpdateImg from './Images/UpdateImg';
import UpdatePostImg from './Images/UpdatePostImg';

const Routes = () => {

    const Auth = React.useContext(AuthApi)

    return (
        <Switch>
            <ProtectedLogin path="/" exact component={Home} />
            <ProtectedLogin path="/signup" component={Signup} />
            <ProtectedLogin path="/login" auth={Auth.auth} component={Login} />
            <ProtectedRoute path="/logout" auth={Auth.auth} component={Logout} />
            <ProtectedRoute path="/posts" auth={Auth.auth} component={Posts} />
            <ProtectedRoute path="/user/:id" auth={Auth.auth} component={User} />
            <ProtectedRoute path="/deleteuser/:id" auth={Auth.auth} component={DeleteAccount} />
            <Route path="/users/:id" auth={Auth.auth} component={UsersPage} />
            <Route path="/updateuser/:id" auth={Auth.auth} component={UpdateAccount} />
            <Route path="/createpost" auth={Auth.auth} component={CreatePost} />
            <Route path="/post/:id" auth={Auth.auth} component={PostPage} />
            <Route path="/updatepost/:id" auth={Auth.auth} component={UpdatePost} />
            <Route path="/updatepost/:id" auth={Auth.auth} component={UpdatePostImg} />
            <Route path="/deletepost/:id" auth={Auth.auth} component={DeletePost} />
            <Route path="/deletecomment/:id" auth={Auth.auth} component={DeleteComment} />
            <Route path="/imageupdate/:id" auth={Auth.auth} component={UpdateImg} />
            <Route path="/deleteuseradmin/:id" auth={Auth.auth} component={DeleteUserAccount} />
        </Switch>
    )
}

const ProtectedLogin = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={() => !auth ? (
                <>
                    <Component />
                </>
            ) :
                (
                    <Redirect to="/posts" />
                )
            }
        />
    )
}

const ProtectedRoute = ({ auth, component: Component, ...rest }) => {
    return (
        <Route
            {...rest}
            render={() => auth ? (
                <>
                    <Component />
                </>
            ) :
                (
                    <Redirect to="/login" />
                )
            }
        />
    )
}

export default Routes;

