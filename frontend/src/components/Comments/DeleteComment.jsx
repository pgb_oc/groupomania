import * as React from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';

class DeleteComment extends React.Component {
    state = { redirection: false };

    constructor(props) {
        super(props)

        const userLog = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: userLog.userId,
            admin: userLog.userAdmin
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault()

        const userLog = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + userLog.token;

        const requestOptions = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        const commentId = this.props.match.params.id;

        fetch((`${process.env.REACT_APP_SERVER_URL}api/comments/` + commentId), requestOptions)
            .then(response => response.json())
            .then((response) => {
                if (response.error) {
                    this.setState({ redirection: true })
                    toast.error("Erreur : Commentaire PAS supprimé.");
                } else {
                    this.setState({ redirection: true })
                    toast.success("Confirmation : Commentaire supprimé.");
                }
            })
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur: ', error);
            });
    }

    render() {
        const postPage = JSON.parse(localStorage.getItem('postPage'));
        const postId = postPage.id;

        const { redirection } = this.state;
        if (redirection) {
            return <Redirect to={'/post/' + postId} />;
        }

        return <>
            <div className="container">
                <h1>Souhaitez vous vraiment supprimer ce commentaire ?</h1>
                <div className="form-submit">
                    <Link to={'/posts/'} className="btn btn-info btn-sm">Retour aux posts</Link>
                    <button className="btn btn-danger btn-sm" onClick={this.handleSubmit}>Supprimer ce commentaire</button>
                </div>
            </div>
        </>
    };
};

export default DeleteComment;