import * as React from 'react';
import { toast } from 'react-toastify';
import { Link, withRouter } from 'react-router-dom';
import Moment from 'react-moment';
import img from '../../images/icon.png';
import Form from 'react-bootstrap/Form';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'

class Comments extends React.Component {

    constructor(props) {
        super(props)
        const userLog = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: userLog.userId,
            postId: '',
            content: undefined,
            comments: [],
            users: []
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        const userLog = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + userLog.token;

        const postId = this.props.match.params.id;
        fetch(`${process.env.REACT_APP_SERVER_URL}api/posts/` + postId + "/comments/",
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    this.setState({ comments: result.data });
                })
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur :', error);
            }
            )

        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/`,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then((result) => {
                this.setState({ users: result.data });
            }
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur :', error);
            }
            )
    }

    handleChange(e) {
        const postPage = JSON.parse(localStorage.getItem('postPage'));
        const postId = postPage.id;

        const userLog = JSON.parse(localStorage.getItem('userLog'));
        const userId = userLog.userId;


        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            postId: postId,
            userId: userId,
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()

        const userLog = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + userLog.token;

        const requestOptions = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        fetch((`${process.env.REACT_APP_SERVER_URL}api/comments/`), requestOptions)
            .then(response => response.json())
            .then((result) =>
                this.setState({ comments: result }),
                toast.success('Votre commentaire à bien été ajouté.')
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error("Erreur :", error);
            });

        this.setState({
            postId: '',
            userId: '',
            comment: '',
        })
    }

    handleBan(comment, e) {
        e.preventDefault()

        const userLog = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + userLog.token;

        const requestOptions = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify({
                postId: comment.postId,
                bannedUserId: comment.userId,
            })
        };

        fetch((`${process.env.REACT_APP_SERVER_URL}api/bans/`), requestOptions)
            .then(response => response.json())
            .then((result) =>
                this.setState({ comments: result }),
                toast.success('L\'utilisateur a été banni.')
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error("Erreur :", error);
            });
    }

    render() {
        const { comments } = this.state;
        const { users } = this.state;
        const userLog = JSON.parse(localStorage.getItem('userLog'));

        return (
            <div className="comment-container">
                <h2>Laisser un commentaire</h2>
                <div className="post-comment">
                    <Form.Group controlId="exampleForm.ControlTextarea1" >
                        <Form.Label>Votre commentaire :</Form.Label>
                        <Form.Control as="textarea" rows={3} name="comment" value={this.state.text} onChange={this.handleChange} />
                    </Form.Group>
                    <div className="form-submit">
                        <button className="btn btn-secondary" onClick={this.handleSubmit}>Envoyer</button>
                    </div>
                </div>
                <h2>Post commenté {comments.length} fois.</h2>
                {comments.map((comment) => (
                    <div className="comment-form" key={"dimg" + comment.id}>
                        {users.map((user) => {
                            if (user.id === comment.userId && user.imageUrl) {
                                return <img src={`${process.env.REACT_APP_SERVER_URL}images/` + user.imageUrl} alt="user" key={"userImg" + comment.id} />
                            } else if (user.id === comment.userId && !user.imageUrl) {
                                return <img src={img} alt="user" key={"userImg" + comment.id} />
                            } else {
                                return null
                            }
                        })}
                        <div className="comment-card" key={"fragment" + comment.id}>
                            {users.map((user) => {
                                if (comment.userId === user.id) {
                                    return <h3 key={"h3" + user.id}>Publié par <Link to={"/users/" + user.id} key={comment.id + user.id} className="nav-link">{user.firstname} {user.lastname}</Link></h3>
                                } else {
                                    return null
                                }
                            })}
                            <p key={"commenttitle" + comment.id}><Moment fromNow key={"date" + comment.id}>{comment.createdAt}</Moment></p>
                            <p key={"comment" + comment.id} className="text-comment">{comment.comment}</p>
                            {comment.userId === userLog.userId || userLog.userAdmin === true
                                ? <div className="post-option">
                                    <Link to={"/deletecomment/" + comment.id} key={"delete" + comment.id} className="delete-comment-icon"><FontAwesomeIcon icon={faTimes} /></Link>
                                </div> : null
                            }
                            {comment.userId !== userLog.userId || userLog.userAdmin === true
                                ? 
                                <button className="btn btn-danger btn-sm" onClick={this.handleBan.bind(this, comment)}>Bannir l'utilisateur</button>
                                : null
                            } 
                        </div>
                    </div>
                ))}
            </div>
        )
    };
};

export default withRouter(Comments);