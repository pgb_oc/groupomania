import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment/locale/fr'  // react moment fr
import Comments from "../Comments/Comments";
import Badge from 'react-bootstrap/Badge'
import img from '../../images/icon.png';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faHeart } from '@fortawesome/free-solid-svg-icons'

function PostPage({ match }) {
    const [error, setError] = useState(null);
    const [loaded, setLoaded] = useState(false);
    const [post, setPost] = useState([]);

    const [likes, setLikes] = useState([]);
    const [users, setUsers] = useState([]);
    const history = useHistory();

    const storage = JSON.parse(localStorage.getItem('userLog'));

    let token = "Bearer " + storage.token;
    let userId = storage.userId;

    let postId = match.params.id;

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/posts/` + postId,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setLoaded(true);
                    setPost(result);
                    localStorage.setItem('postPage', JSON.stringify(result));
                },
                (error) => {
                    setLoaded(true);
                    setError(error);
                }
            )
    }, [postId, token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/`,
            {
                headers:
                    { "Authorization": token },
                    credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setLoaded(true);
                    setUsers(result.data);
                },
                (error) => {
                    setLoaded(true);
                    setError(error);
                }
            )
    }, [token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/posts/` + postId + "/likes/",
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setLoaded(true);
                    setLikes(result.data.length);
                },
                (error) => {
                    setLoaded(true);
                    setError(error);
                }
            )
    }, [postId, token])

    function LikeSubmit() {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/likes/`, {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify({
                postId: postId,
                userId: userId,
                like: 1
            })
        })
            .then(res => res.json())
            .then(
                (result) => {
                    setLikes(result.like)
                    setLoaded(true);
                }, (error) => {
                    if (error) {
                        setError(error);
                    }
                })
    }

    let userAuth;
    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!loaded) {
        return <div className="loading-spinner"></div>
    } else if (post.userId === storage.userId) {
        userAuth = <div className="post-button">
            <button className="btn btn-secondary btn-sm" onClick={() => { history.push("/updatepost/" + postId) }}>Modifier</button>
            <button className="btn btn-danger btn-sm" onClick={() => { history.push("/deletepost/" + postId) }}>Supprimer</button>
        </div>
    } else if (storage.userAdmin === true) {
        userAuth = <div className="post-button">
            <button className="btn btn-danger btn-sm" onClick={() => { history.push("/deletepost/" + postId) }}>Supprimer</button>
        </div>
    }

    return (
        <>
            <div className="container">
                <div className="post-container">
                    {users.map((user) => {
                        if (user.id === post.userId && user.imageUrl) {
                            return <img src={`${process.env.REACT_APP_SERVER_URL}images/` + user.imageUrl} alt="user" key={"userImg" + post.id} />
                        } else if (user.id === post.userId && !user.imageUrl) {
                            return <img src={img} alt="user" key={"userImg" + post.id} />
                        } else {
                            return null
                        }
                    })}
                    <div className="post-text">
                        {users.map((user) => {
                            if (post.userId === user.id) {
                                return <h2 key={"h2" + user.id}>Publié par <Link to={"/users/" + user.id} key={user.id + post.id} className="post-link">{user.firstname} {user.lastname}</Link></h2>
                            } else {
                                return null
                            }
                        })}
                        <h2>{post.title} </h2>
                        <p key={"text" + post.id}>{post.text}</p>
                        <p><Moment locale="fr" fromNow key={"date" + post.id}>{post.createdAt}</Moment></p>
                        <div className="post-page">
                            <div className="show-article">
                            </div>
                            <div className="post-img">
                                {post.postUrl ?
                                    <img
                                        //src={`${process.env.REACT_APP_SERVER_URL}images/` + post.postUrl}
                                        src={post.postUrl}
                                        alt="post"
                                        key={"postImage" + post.id}
                                    />
                                    : null
                                }
                            </div>
                            {userAuth}
                        </div>
                        <div className="likes">
                            <button onClick={LikeSubmit}>
                                <Badge pill variant="danger">
                                    <FontAwesomeIcon className="likes-icon" icon={faHeart} /> {likes}
                                </Badge>
                            </button>
                        </div>
                    </div>
                </div>
                <Comments />
            </div>
        </>
    );
};

export default PostPage;