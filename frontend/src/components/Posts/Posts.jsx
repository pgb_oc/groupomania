import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment/locale/fr'  // react moment fr
import defaultimg from '../../images/default-user.png';
//import CreatePost from "./CreatePost";

const Posts = () => {
    const [error, setError] = useState(null);
    const [loaded, setLoaded] = useState(false);
    const [posts, setPosts] = useState([]);
    const [users, setUsers] = useState([]);
    const history = useHistory();

    const storage = JSON.parse(localStorage.getItem('userLog'));
    let token = "Bearer " + storage.token;

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/posts`,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            
            .then(res => res.json())
            .then(
                (result) => {
                    setLoaded(true);
                    setPosts(result.data);
                },
                (error) => {
                    setLoaded(true);
                    setError(error);
                }
            )
    }, [token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users`,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setLoaded(true);
                    setUsers(result.data);
                },
                (error) => {
                    setLoaded(true);
                    setError(error);
                }
            )
    }, [token])

    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!loaded) {
        return <div className="loading-spinner"></div>
    } else {
        return (
            <>

                <div className="container">
                    <h1>Tous les posts actifs</h1>

                    <div className="form-submit">
                        <button className="btn btn-secondary btn-sm" onClick={() => { history.push("/createpost/") }}>Créer un post</button>

                    </div>
                    <div className='appear-animation fadeinup'>
                        {posts.map((post) => (
                            <div className="post-card" key={"postCard" + post.id}>
                                {users.map((user) => {
                                    if (user.id === post.userId && user.imageUrl) {
                                        return <img src={`${process.env.REACT_APP_SERVER_URL}images/` + user.imageUrl} alt="user" key={"userImg" + post.id} />
                                    } else if (user.id === post.userId && !user.imageUrl) {
                                        return <img src={defaultimg} alt="user" key={"userImg" + post.id} />
                                    } else {
                                        return null
                                    }
                                })}
                                <div className="show-article" key={"show" + post.id}>
                                    {users.map((user) => {
                                        if (user.id === post.userId) {
                                            return <h2 key={"h2" + user.id}>Publié par <Link to={"/users/" + user.id} key={user.id + post.id} className="post-link">{user.firstname} {user.lastname}</Link></h2>
                                        } else {
                                            return null
                                        }
                                    })}
                                    <Link to={"/post/" + post.id} key={"post" + post.id} className="post-link">{post.title}</Link>
                                    <p key={"text" + post.id}>{post.text}</p>
                                    <p key={post.createdAt} id="created-at"><Moment locale="fr" fromNow key={"date" + post.id}>{post.createdAt}</Moment></p>
                                </div>
                            </div>
                        ))}
                    </div>
                </div>
            </>
        );
    }
};

export default Posts;
