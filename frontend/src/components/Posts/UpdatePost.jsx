import * as React from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';
import Field from '../Form/Field';
import Form from 'react-bootstrap/Form'

class UpdatePost extends React.Component {

    state = { redirection: false };

    constructor(props) {
        super(props)
        const postPage = JSON.parse(localStorage.getItem('postPage'));
        const storage = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: storage.userId,
            admin: storage.userAdmin,
            title: postPage.title,
            text: postPage.text,
            postUrl: postPage.postUrl
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()

        const storage = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + storage.token;

        const requestOptions = {
            method: 'put',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        let postPage = JSON.parse(localStorage.getItem('postPage'));
        let postId = postPage.id;

        fetch((`${process.env.REACT_APP_SERVER_URL}api/posts/` + postId), requestOptions)
            .then(response => response.json())
            .then((response) => {
                if (response.error) {
                    toast.error('Erreur :' + response.error);
                } else {
                    this.setState({ redirection: true });
                    toast.success("Votre post à bien été modifié.");
                }
            }
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                toast.error('Erreur :' + error);
            });
    }

    render() {
        const { redirection } = this.state;
        const postId = this.props.match.params.id;
        if (redirection) {
            return <Redirect to={'/post/' + postId} />;
        }

        return <>
            <div className="container">
                <h1>Modifier ce post</h1>
                <form>
                    <Field name="title" value={this.state.title} onChange={this.handleChange}>Titre</Field>
                    <Form.Group controlId="exampleForm.ControlTextarea1" >
                        <Form.Label>Description du post</Form.Label>
                        <Form.Control as="textarea" rows={8} name="text" value={this.state.text} onChange={this.handleChange} />
                    </Form.Group>
                    <Field name="postUrl" value={this.state.postUrl} onChange={this.handleChange}>Partagez un lien pour ce post</Field>
                    <div className="form-submit">
                        <button className="btn btn-success btn-sm" onClick={this.handleSubmit}>Enregistrer les modifications</button>
                        <Link to='/posts/' className="btn btn-info btn-sm">Retour aux posts</Link>
                    </div>
                </form>
            </div>
        </>
    };
};

export default UpdatePost;