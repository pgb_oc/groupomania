import * as React from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';

class DeletePost extends React.Component {
    state = { redirection: false };

    constructor(props) {
        super(props)
        const userLog = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: userLog.userId,
            admin: userLog.userAdmin
        }

        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault()

        const storage = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + storage.token;

        const requestOptions = {
            method: 'delete',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        let postPage = JSON.parse(localStorage.getItem('postPage'));
        let postId = postPage.id

        fetch((`${process.env.REACT_APP_SERVER_URL}api/posts/` + postId), requestOptions)
            .then(response => response.json())
            .then(
                (response) => {
                    if (response.error) {
                        this.setState({ redirection: true });
                        toast.error('Une erreur est survenue, votre post n\'a pas été supprimé.');
                    } else {
                        this.setState({ redirection: true });
                        toast.success('Votre post à bien été supprimé.');
                    }
                }
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur :', error);
            }
            );
    }

    render() {
        const { redirection } = this.state;
        if (redirection) {
            return <Redirect to='/posts' />;
        }

        return <>
            <div className="container">
                <h1>Souhaitez-vous supprimer ce post définitivement ?</h1>
                <div className="form-submit">
                    <Link to={'/posts/'} className="btn btn-info btn-sm">Retour aux posts</Link>
                    <button className="btn btn-danger btn-sm" onClick={this.handleSubmit}>Supprimer ce post</button>
                </div>
            </div>
        </>
    };
};

export default DeletePost;