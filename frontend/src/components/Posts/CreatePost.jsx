import * as React from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';
//import UpdatePostImg from '../Images/UpdatePostImg';
import Field from '../Form/Field';
import Form from 'react-bootstrap/Form'

class CreatePost extends React.Component {

    state = { redirection: false };

    constructor(props) {
        super(props)

        const userLog = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: userLog.userId,
            admin: userLog.userAdmin,
            title: undefined || '',
            text: undefined || '',
            postUrl: undefined || ''
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()

        const storage = JSON.parse(localStorage.getItem('userLog'));
        let token = "Bearer " + storage.token;

        const requestOptions = {
            method: 'post',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        fetch((`${process.env.REACT_APP_SERVER_URL}api/posts/`), requestOptions)
            .then(response => response.json())
            .then(
                (response) => {
                    if (response.error) {
                        console.log(response.error);
                        toast.error('Tous les champs sont obligatoires.');
                    } else {
                        this.setState({ redirection: true })
                        toast.success('Votre post vient d\'être publié.');
                    }
                })
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur', error);
            });
    }

    render() {
        const { redirection } = this.state;
        if (redirection) {
            return <Redirect to='/posts' />;
        }

        return <>
            <div className="container">
                <h1>Créer un post</h1>
                <form>
                    <Field name="title" value={this.state.title} onChange={this.handleChange}>Titre du post</Field>
                    <Form.Group controlId="exampleForm.ControlTextarea1" >
                        <Form.Label>Description du post</Form.Label>
                        <Form.Control as="textarea" rows={8} name="text" value={this.state.text} onChange={this.handleChange} />
                    </Form.Group>

                    <Field name="postUrl" value={this.state.postUrl} onChange={this.handleChange}>URL de l'image</Field>
                    <p>Vous pouvez utiliser un service d'hébergement d'image comme https://www.zupimages.net/<br/>
                    Puis coller dans le champ URL de l'image le Lien direct de votre image.<br/>
                    Lien d'exemple: https://zupimages.net/up/21/52/3md6.png
                    </p>
                    <div className="form-submit">
                        <button className="btn btn-secondary btn-sm" onClick={this.handleSubmit}>Créer le post</button>
                        <Link to='/posts' className="btn btn-primary btn-sm">Retour aux posts</Link>
                    </div>
                </form>
            </div>
        </>
    };
};

export default CreatePost;