import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Moment from 'react-moment';
import 'moment-timezone';
import img from '../../images/default-user.png';

const UsersPage = ({ match }) => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [user, setUser] = useState([]);
    const [posts, setPost] = useState([]);
    const history = useHistory();

    const storage = JSON.parse(localStorage.getItem('userLog'));
    const userId = match.params.id;
    let token = "Bearer " + storage.token;

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setUser(result);
                    localStorage.setItem('userData', JSON.stringify(result));
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [userId, token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId + "/posts/",
            {
                headers:
                    { "Authorization": token },
                    credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setPost(result.data);
                    localStorage.setItem('userPosts', JSON.stringify(result.data));
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [userId, token])


    let idUser;
    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div className="loading-spinner"></div>
    } else if (storage.userAdmin === true) {
        idUser = <div className="user-button">
            <button className="btn btn-danger btn-sm" onClick={() => { history.push("/deleteuseradmin/" + userId) }}>Supprimer le compte</button>
        </div>
    }

    return (
        <>
            <div className="container">
                <h1>{user.firstname} {user.lastname}</h1>
                <div className="user-container appear-animation fadeinup">
                    <div className="images">
                        {user.imageUrl ?
                            <img
                                src={`${process.env.REACT_APP_SERVER_URL}images/` + user.imageUrl}
                                alt="users"
                                key={"userImage" + user.id}
                            /> :
                            <img
                                src={img}
                                alt="user"
                                key={"userImg" + user.id}
                            />
                        }
                    </div>
                    <div className="display-post">
                        <p>Présentation de {user.firstname} :<br />
                            {user.desc}</p>
                    </div>
                    {idUser}
                </div>
                <div className="user-post">
                    <h2>Posts créés par {user.firstname}</h2>
                    {posts.map((post) => (
                        <div className="user-posts" key={"posts" + post.id}>
                            <Link to={"/post/" + post.id} key={"post" + post.id} className="post-link">{post.title}</Link>
                            <p key={"posttext" + post.id}>{post.text}</p>
                            <h3 key={"dateid" + post.id}>Créé le <Moment locale="fr" key={"date" + post.id} format="DD MMM YYYY" date={post.createdAt} /></h3>
                        </div>
                    ))}
                </div>
            </div>
        </>
    );
};

export default UsersPage;