import React, { useState, useCallback } from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';

function DeleteUserAccount({ match }) {
    const [redirect, setRedirect] = useState(false);
    const storage = JSON.parse(localStorage.getItem('userLog'));
    let token = "Bearer " + storage.token;
    let userId = match.params.id;

    const handleSubmit = useCallback(function (value) {

        fetch((`${process.env.REACT_APP_SERVER_URL}api/users/` + userId), {
            method: "delete",
            headers:
            {
                "Content-type": 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify({
                id: value.id,
                userId: storage.userId,
                admin: storage.userAdmin
            })
        })
            .then(res => res.json())
            .then(
                (res) => {
                    if (res.error) {
                        toast.error('Erreur : Impossible de supprimer ce compte.');
                    } else {
                        toast.success('Confirmation : Compte supprimé.');
                        setRedirect(true);
                    }
                }
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                toast.error('Erreur : Impossible de supprimer ce compte.');
                console.error('Erreur :', error);
            })
    }, [userId, storage.userAdmin, storage.userId, token])

    return (
        <>
            {redirect ? <Redirect to="/posts/" /> : null}
            <div className="container">
                <h1>Êtes-vous sûr de vouloir supprimer definitivement ce compte ?</h1>
                <div className="form-submit">
                    <Link to={'/user/' + userId} className="btn btn-info btn-sm">Retour au compte</Link>
                    <button className="btn btn-danger btn-sm" onClick={handleSubmit}>Supprimer ce compte</button>
                </div>
            </div>
        </>
    );
}

export default DeleteUserAccount;