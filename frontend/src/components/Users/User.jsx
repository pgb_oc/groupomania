import React, { useState, useEffect } from 'react';
import { Link, useHistory } from 'react-router-dom';
import Moment from 'react-moment';
import UpdateImg from '../Images/UpdateImg';

import Logout from '../Auth/Logout';


const User = () => {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [user, setUser] = useState([]);
    const [posts, setPost] = useState([]);
    const history = useHistory();

    const storage = JSON.parse(localStorage.getItem('userLog'));
    const userId = storage.userId;
    let token = "Bearer " + storage.token;

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId,
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setUser(result);
                    localStorage.setItem('userData', JSON.stringify(result));
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [userId, token])

    useEffect(() => {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId + "/posts/",
            {
                headers:
                    { "Authorization": token },
                credentials: "include",
            })
            .then(res => res.json())
            .then(
                (result) => {
                    setIsLoaded(true);
                    setPost(result.data);
                },
                (error) => {
                    setIsLoaded(true);
                    setError(error);
                }
            )
    }, [userId, token])

    let idUser;
    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else if (!isLoaded) {
        return <div className="loading-spinner"></div>

    } else if (user.id === userId || user.admin === true) {
        idUser = <div className="user-button">
            <button className="btn btn-primary btn-lg" onClick={() => { history.push("/updateuser/" + userId) }}>Modifier le compte</button>
            <><Logout /></>
            <button className="btn btn-danger btn-sm" onClick={() => { history.push("/deleteuser/" + userId) }}>Supprimer le compte</button>
        </div>
    }

    return (
        <>
            <div className="container">
                <h1>Bienvenue {user.firstname} !</h1>
                {storage.userAdmin === true ?
                    <p className="admin-account">Compte Administrateur</p> : <></>}
                <div className="user-container appear-animation fadeinup">
                    <div className="images">
                        <>
                            <UpdateImg />
                        </>
                    </div>
                    {idUser}
                </div>

                <div className='container'>
                    <div className="display-post appear-animation fadeinup">
                        <h2>{user.firstname} {user.lastname}</h2>
                        <p>{user.desc}</p>
                    </div>
                </div>
                <div className=' appear-animation fadeinup'>
                    <div className="user-post">
                        <h2>Vos posts :</h2>
                        {posts.map((post) => (
                            <div className="user-posts" key={"user" + post.id}>
                                <Link to={"/post/" + post.id} key={"post" + post.id} className="post-link">{post.title}</Link>
                                <p key={"posttext" + post.id}>{post.text}</p>
                                <h3 key={"date" + post.id}>Post créé le <Moment key={"date" + post.id} format="DD MMM YYYY" date={post.createdAt} /></h3>
                            </div>
                        ))}
                    </div>
                </div>
            </div>
        </>
    );
};

export default User;

