import * as React from 'react';
import { toast } from 'react-toastify';
import { Redirect, Link } from 'react-router-dom';
import Field from '../Form/Field';
import Form from 'react-bootstrap/Form'

class UpdateAccount extends React.Component {

    state = { redirection: false };

    constructor(props) {
        super(props)
        const userData = JSON.parse(localStorage.getItem('userData'));
        const userLog = JSON.parse(localStorage.getItem('userLog'));

        this.state = {
            userId: userLog.userId,
            admin: userLog.userAdmin,
            firstname: userData.firstname,
            lastname: userData.lastname,
            desc: userData.desc || '',
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleChange(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()

        const storage = JSON.parse(localStorage.getItem('userLog'));
        const userId = storage.userId
        let token = "Bearer " + storage.token;

        const requestOptions = {
            method: 'put',
            headers: {
                "Content-type": 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify(this.state)
        };

        fetch((`${process.env.REACT_APP_SERVER_URL}api/users/` + userId), requestOptions)
            .then(response => response.json())
            .then((response) => {
                if (response.error) {
                    toast.error('Erreur : Impossible de modifier ce compte.' + response.error);
                } else {
                    this.setState({ redirection: true });
                    toast.success('Compte modifié');
                }
            })
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                console.error('Erreur :', error);
            });
    }

    render() {
        const userData = JSON.parse(localStorage.getItem('userData'));
        const userId = userData.id;

        const { redirection } = this.state;

        if (redirection) {
            return <Redirect to={'/user/' + userId} />;
        }

        return <>
            <div className="container">
                <h1>Modifier mon profile</h1>
                <form>
                    <Field name="firstname" value={this.state.firstname} onChange={this.handleChange}>Votre prénom</Field>
                    <Field name="lastname" value={this.state.lastname} onChange={this.handleChange}>Votre nom</Field>
                    <Form.Group controlId="exampleForm.ControlTextarea1" >
                        <Form.Label>Présentation :</Form.Label>
                        <Form.Control as="textarea" rows={5} name="desc" value={this.state.desc} onChange={this.handleChange} />
                    </Form.Group>
                    <div className="form-submit">
                        <button className="btn btn-success btn-sm" onClick={this.handleSubmit}>Enregistrer les modifications</button>
                        <Link to={'/user/' + userId} className="btn btn-info btn-sm">Retour à mon compte</Link>
                    </div>
                </form>
            </div>
        </>
    };
};

export default UpdateAccount;