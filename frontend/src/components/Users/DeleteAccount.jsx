import React, { useCallback } from 'react';
import { toast } from 'react-toastify';
import { Link } from 'react-router-dom';
import AuthApi from '../AuthApi';
import Cookies from 'js-cookie';

function DeleteAccount() {
    const Auth = React.useContext(AuthApi);

    const storage = JSON.parse(localStorage.getItem('userLog'));
    const userId = storage.userId;
    const admin = storage.userAdmin;
    let token = "Bearer " + storage.token;

    const handleSubmit = useCallback(function (value) {


        fetch((`${process.env.REACT_APP_SERVER_URL}api/users/` + userId), {
            method: "delete",
            headers:
            {
                "Content-type": 'application/json',
                'Authorization': token
            },
            credentials: "include",
            body: JSON.stringify({
                id: value.id,
                userId: userId,
                admin: admin
            })
        })
            .then(res => res.json())
            .then(
                (res) => {
                    if (res.error) {
                        toast.error('Erreur : Impossible de supprimer ce compte.');
                    } else {
                        toast.success('Confirmation : Compte supprimé.');
                        Auth.setAuth(false);
                        Cookies.remove("user");
                        localStorage.clear();
                    }
                }
            )
            .catch(error => {
                this.setState({ Erreur: error.toString() });
                toast.error('Erreur : Impossible de supprimer ce compte.');
                console.error('Erreur :', error);
            })
    }, [Auth, admin, userId, token])

    return (
        <div className="container">
            <h1>Êtes-vous sûr de vouloir supprimer definitivement votre compte ?</h1>
            <div className="form-submit">
                <Link to={'/user/' + userId} className="btn btn-info btn-sm">Retour au compte</Link>
                <button className="btn btn-danger btn-sm" onClick={handleSubmit}>Supprimer le compte</button>
            </div>
        </div>
    );
}

export default DeleteAccount;