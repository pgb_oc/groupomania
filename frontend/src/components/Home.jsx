import React from 'react';
import { Link } from 'react-router-dom';
import logohome from '../images/logo-home.png';

const Home = () => {
    return (
        <>
            <div className="container">
                <h1>Bienvenue sur la plateforme sociale Groupomania</h1>
                <div className='appear-animation fadeinup'>
                    <div className='logo-home-container appear-animation fadeinup'>
                        <img className="logo-home" src={logohome} alt="logo home" />
                    </div>
                </div>
                <p className="home-text"><Link to="/login">Cliquez-ici pour vous connecter</Link><br />
                    <br />
                    Vous n'avez pas encore de compte ? <Link to="/signup">Inscrivez-vous !</Link></p>
            </div>
        </>
    );
}

export default Home;