import React from 'react';
import { toast } from 'react-toastify';
//import { Link, Redirect } from 'react-router-dom';
import Button from 'react-bootstrap/Button';

class UpdatePostImg extends React.Component {

    state = { redirection: false }

    constructor(props) {
        const storage = JSON.parse(localStorage.getItem('userLog'));

        super(props)
        this.state = {
            userId: storage.userId,
            admin: storage.userAdmin,
            title: undefined || '',
            text: undefined || '',
            postUrl: undefined || '',
            redirect: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        const name = e.target.name;
        const value = e.target.value;
        this.setState({
            [name]: value
        })
    }
    handleSubmit(e) {
        e.preventDefault();

        const formData = new FormData();
        const imgData = document.querySelector('input[type="file"]').files[0];
        formData.append('image', imgData);

        const storage = JSON.parse(localStorage.getItem('userLog'));
        const userId = storage.userId;
        let token = "Bearer " + storage.token;

        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId,
            {
                method: 'put',
                headers: { "Authorization": token },
                credentials: "include",
                body: formData
            })
            .then((res) => {
                this.setState({ redirect: true })
                if (res.ok) {
                    toast.success("Confirmation: Image ajoutée.");
                } else if (res.status === 401) {
                    toast.error("Erreur 401");
                }
            }, function (err) {
                toast.error("Erreur : " + err);
            });
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        const storageImg = JSON.parse(localStorage.getItem('userPosts')) || {};
        const postId = storageImg.id;
        const postUrl = storageImg.postUrl;

        let { imagePreviewUrl } = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (
                <img
                    src={imagePreviewUrl}
                    alt="user"
                    key={"postImage" + postId}
                />);
        }
        else {
            $imagePreview = (<img
                src={`${process.env.REACT_APP_SERVER_URL}images/` + postUrl}
                alt="user"
                key={"postImage" + postId}
            />);
        }

        return <div className="container">
            <h2>Ajouter une image au post</h2>
            <div className='post-img-prev'>
                <div className="imgPreview">
                    {$imagePreview}
                </div>
            </div>
            <div className="addPhotoForm" onSubmit={(e) => this.handleSubmit(e)}>
                <input className="form-control" type="file" name="postUrl" value={this.state.postUrl} onChange={(e) => this.handleImageChange(e)} />
                <Button className="submitButton submitImg" color="success" type="Submit" onClick={(e) => this.handleSubmit(e)}>Ajouter l'image</Button>
            </div>
        </div>
    }
}

export default UpdatePostImg;