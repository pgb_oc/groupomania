import React from 'react';
import { toast } from 'react-toastify';
import Button from 'react-bootstrap/Button';
import img from '../../images/default-user.png';

class UpdateImg extends React.Component {

    state = { redirection: false }

    constructor(props) {
        const storage = JSON.parse(localStorage.getItem('userLog'));

        super(props)
        this.state = {
            userId: storage.userId,
            admin: storage.userAdmin,
            redirect: false
        }
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();

        const formData = new FormData();
        const imgData = document.querySelector('input[type="file"]').files[0];
        formData.append('image', imgData);

        const storage = JSON.parse(localStorage.getItem('userLog'));
        const userId = storage.userId;
        let token = "Bearer " + storage.token;

        fetch(`${process.env.REACT_APP_SERVER_URL}api/users/` + userId,
            {
                method: 'put',
                headers: { "Authorization": token },
                credentials: "include",
                body: formData
            })
            .then((res) => {
                this.setState({ redirect: true })
                if (res.ok) {
                    toast.success("Confirmation: Image modifiée.");
                } else if (res.status === 401) {
                    toast.error("Erreur 401");
                }
            }, function (err) {
                toast.error("Erreur : " + err);
            });
    }

    handleImageChange(e) {
        e.preventDefault();

        let reader = new FileReader();
        let file = e.target.files[0];

        reader.onloadend = () => {
            this.setState({
                file: file,
                imagePreviewUrl: reader.result
            });
        }

        reader.readAsDataURL(file)
    }

    render() {
        const storage = JSON.parse(localStorage.getItem('userLog'));
        const storageImg = JSON.parse(localStorage.getItem('userData')) || {};
        const userId = storage.userId;
        const imageUrl = storageImg.imageUrl;

        let { imagePreviewUrl } = this.state;
        let $imagePreview = null;
        if (imagePreviewUrl) {
            $imagePreview = (
                <img
                    src={imagePreviewUrl}
                    alt="user"
                    key={"userImage" + userId}
                />);
        }
        else if (imageUrl) {
            $imagePreview = (<img
                src={`${process.env.REACT_APP_SERVER_URL}images/` + imageUrl}
                alt="user"
                key={"userImage" + userId}
            />);
        }
        else {
            $imagePreview = (<img
                src={img}
                alt="user"
                key={"userImage" + userId}
            />);
        }

        return <div className="container">
            <h2 className='user-title'>Modifiez votre photo</h2>
            <div className="imgPreview">
                {$imagePreview}
            </div>
            <form className="addPhotoForm" onSubmit={(e) => this.handleSubmit(e)}>
                <input className="form-control" type="file" name="imageUrl" onChange={(e) => this.handleImageChange(e)} />
                <Button className="submitButton" color="success" type="Submit" onClick={(e) => this.handleSubmit(e)}>Modifier l'image</Button>
            </form>
        </div>

    }
}

export default UpdateImg;