import React from "react";
import Cookies from 'js-cookie';
import { toast } from 'react-toastify';

  const Logout = () => {
    const deleteCookie = () => {
      Cookies.remove("groupo_sess");
};

  const logout = async () => {
    Cookies.remove("user");
    window.localStorage.clear();
    fetch(`${process.env.REACT_APP_SERVER_URL}api/auth/logout/`, {
      method: "get",
      headers: { "Content-type": 'application/json' },
      credentials: "include",
  }) 
      .then(() => deleteCookie(),
      toast.success('Vous êtes maintenant déconnecté.'))
      .catch((err) => console.log(err));
    window.location = "/";

  };

  return (
    <button className="btn btn-secondary btn-lg" onClick={logout}>Déconnexion</button>
  );
};

export default Logout;