import React from 'react';
import { toast } from 'react-toastify';
import { Redirect } from 'react-router-dom';

class Signup extends React.Component {

    state = { redirection: false }

    constructor(props) {
        super(props)
        this.state = {
            firstname: undefined || '',
            lastname: undefined || '',
            email: undefined || '',
            password: undefined || '',
            errors: {
                firstname: undefined || '',
                lastname: undefined || '',
                email: undefined || '',
                password: undefined || '',
            }
        }
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handleChange(e) {
        e.preventDefault();
        const name = e.target.name;
        const value = e.target.value;
        let errors = this.state.errors;
        const checkEmailRegex = RegExp(/*/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[#?!@$%^&*-])[A-Za-z\d@$!%*?&]{8,}$/gm*/);

        switch (name) {
            case 'firstname':
                errors.firstname =
                    value.length < 2
                        ? 'Votre prénom doit être composé d\'un minimum de 2 caractères.'
                        : '';
                break;
            case 'lastname':
                errors.lastname =
                    value.length < 2
                        ? 'Votre nom doit être composé d\'un minimum de 2 caractères.'
                        : '';
                break;
            case 'email':
                errors.email = checkEmailRegex.test(value)
                    ? ''
                    : 'Votre email est invalide (email exemple : nom@groupomania.com)'
                break;
            case 'password':
                errors.password =
                    value.length < 8
                        ? 'Mot de passe invalide, votre mot de passe doit faire au minimum 8 caractères.'
                        : '';
                break;
            default:
                break;
        }

        this.setState({
            errors,
            [name]: value
        })
    }

    handleSubmit(e) {
        e.preventDefault()

        const checkForm = (errors) => {
            let valid = true;
            Object.values(errors).forEach(
                (val) => val.length > 0 && (valid = false)
            );
            return valid;
        }

        if (checkForm(this.state.errors)) {
            console.info('Formulaire valide !')
            console.log(this.state)
            const requestOptions = {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer token',
                },
                credentials: "include",
                body: JSON.stringify(this.state)
            };
            fetch(`${process.env.REACT_APP_SERVER_URL}api/auth/signup`, requestOptions)
                .then(response => response.json())
                .then((response) => {
                    if (response.error) {
                        alert("Erreur : " + response.error);
                    } else {
                        this.setState({ redirection: true })
                        toast.success('Votre compte vient d\'être créé. Merci de vous connecter.');
                    }
                })
                .catch(error => {
                    this.setState({ Erreur: error.toString() });
                    toast.error('Vous devez renseigner tous les champs.');
                    console.error('Erreur :', error);
                });

        } else {
            toast.error('Formulaire non valide. Merci de contrôler vos informations.');
        }
    }

    render() {
        const { redirection } = this.state;
        if (redirection) {
            return <Redirect to='/login' />;
        }

        const { errors } = this.state;

        return <>
            <div className="container">
                <h1>Inscrivez au réseau social de votre entreprise !</h1>
                <form>
                    <div className="form-group">
                        <label htmlFor="firstname">Prénom</label>
                        <input type="text" value={this.state.firstname} onChange={this.handleChange} name="firstname" className="form-control" required noValidate />
                        {errors.firstname.length > 0 &&
                            <span className='error'>{errors.firstname}</span>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="lastname">Nom</label>
                        <input type="text" value={this.state.lastname} onChange={this.handleChange} name="lastname" className="form-control" required noValidate />
                        {errors.lastname.length > 0 &&
                            <span className='error'>{errors.lastname}</span>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="email">Email</label>
                        <input type="text" value={this.state.email} onChange={this.handleChange} name="email" className="form-control" required noValidate />
                        {errors.email.length > 0 &&
                            <span className='error'>{errors.email}</span>}
                    </div>
                    <div className="form-group">
                        <label htmlFor="password">Mot de passe</label>
                        <input type="password" value={this.state.password} onChange={this.handleChange} name="password" className="form-control" required noValidate />
                        {errors.password.length > 0 &&
                            <span className='error'>{errors.password}</span>}
                    </div>
                    <div className="form-submit">
                        <button className="btn-secondary btn-lg" onClick={this.handleSubmit} noValidate>Valider mon inscription</button>
                    </div>
                </form>
            </div>
        </>
    };
};

export default Signup;