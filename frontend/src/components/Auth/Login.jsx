import React, { useState, useCallback, useContext, useMemo, createContext } from 'react';
import { toast } from 'react-toastify';
import AuthApi from '../AuthApi';
import Cookies from 'js-cookie';

const FormContext = createContext({})

function FormWithContext({ defaultValue, onSubmit, children }) {

    const [data, setData] = useState(defaultValue)

    const change = useCallback(function (name, value) {
        setData(d => ({ ...d, [name]: value }))
    }, [])

    const value = useMemo(function () {
        return { ...data, change }
    }, [data, change])

    const handleSubmit = useCallback(function (e) {
        e.preventDefault()
        onSubmit(value)
    }, [onSubmit, value])

    return <FormContext.Provider value={value}>
        <form onSubmit={handleSubmit}>
            {children}
        </form>
    </FormContext.Provider>
}

function FormField({ name, type, children }) {
    const data = useContext(FormContext)
    const handleChange = useCallback(function (e) {
        data.change(e.target.name, e.target.value)
    }, [data])

    return <div className="form-group">
        <label htmlFor={name}>{children}</label>
        <input type={type} name={name} id={name} className="form-control" value={data[name] || ''} onChange={handleChange} />
    </div>
}

function PrimaryButton({ children }) {
    return <div className="form-submit">
        <button className="btn-secondary btn-lg">{children}</button>
    </div>
}

function Login() {
    localStorage.clear();
    const [error, setError] = useState(null);
    const Auth = React.useContext(AuthApi);

    const handleSubmit = useCallback(function (value) {
        fetch(`${process.env.REACT_APP_SERVER_URL}api/auth/login/`, {
            method: "post",
            headers: { "Content-type": 'application/json' },
            body: JSON.stringify({
                email: value.email,
                password: value.password
            }),
            credentials: "include",
        })
            .then(res => res.json())

            .then(
                (result) => {
                    localStorage.setItem('userLog', JSON.stringify(result));
                    let storage = JSON.parse(localStorage.getItem('userLog'));

                    if (storage.token === undefined) {
                        Auth.setAuth(false)
                        toast.error('Erreur d\'authentification. Connectez-vous ou inscrivez-vous. !');
                    } else {
                        Auth.setAuth(true)
                        Cookies.set("user", "loginTrue")
                        toast.success('Nous sommes heureux de vous revoir !');
                    }
                },
                (error) => {
                    if (error) {
                        setError(error);
                        Auth.setAuth(false)
                    }
                }
            )
    }, [Auth])

    if (error) {
        return <div>Erreur : {error.message}</div>;
    } else {
        return (
            <>
                <div className="container">
                    <h1>Connectez-vous à votre compte</h1>
                    <FormWithContext onSubmit={handleSubmit}>
                        <FormField name="email" type="text">Votre adresse email</FormField>
                        <FormField name="password" type="password">Votre mot de passe</FormField>
                        <PrimaryButton>Espace membre</PrimaryButton>
                    </FormWithContext>
                </div>
            </>
        );
    }
}

export default Login;