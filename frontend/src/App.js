import React from 'react';
import { BrowserRouter as Router, Link } from 'react-router-dom';
import Cookies from 'js-cookie';
import Routes from './components/Routes';
import AuthApi from './components/AuthApi';
import logo from './images/icon.png';


// Styles
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Navbar from 'react-bootstrap/Navbar';
import Nav from "react-bootstrap/Nav";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faGlobe } from '@fortawesome/free-solid-svg-icons'

import './css/App.css';
// import './scss/App.scss';

// Composant App
function App() {
  // New state variable declaration "auth"
  const [auth, setAuth] = React.useState(false); 

  // Cookies
  const readCookie = () => {
    const user = Cookies.get("user");
    if (user) {
      setAuth(true);
    }
  }

  React.useEffect(() => {
    readCookie();
  }, [])

  // Navbar

  let navLink;
  if (auth === true) {
    const storage = JSON.parse(localStorage.getItem('userLog'));
    const userId = storage.userId;
    navLink = <>
      <Nav>
        <Link to="/posts" className="nav-icon-post">
          <div className='appear-animation fadeinup'>
            <FontAwesomeIcon icon={faGlobe} />
          </div>
        </Link>
        <Link to={"/user/" + userId} className="nav-icon">
          <div className='appear-animation fadeinup'>
            <FontAwesomeIcon icon={faUser} />
          </div>
        </Link>
      </Nav>
    </>

  } else {
    navLink = <Nav className="navbar">
      <Link to="/signup" className="nav-link">
        <div className='appear-animation fadeinup'>
          S'inscrire
        </div>
      </Link>
      <Link to="/login" className="nav-link">
        <div className='appear-animation fadeinup'>
          Se connecter
        </div>
      </Link>
    </Nav>
  }

  return (
    <>
      <AuthApi.Provider value={{ auth, setAuth }}>
        <Router>
          <Navbar sticky="top" className='bg-blue'>
            <Link to="/" className="logo">
              <div className='appear-animation fadeinup'>
                <img src={logo} alt="logo" /><h1 className="text-logo">Groupomania</h1>
              </div>
            </Link>
            {navLink}
          </Navbar>
          <Routes />
        </Router>
        <ToastContainer />
      </AuthApi.Provider>
    </>
  );
}

export default App;