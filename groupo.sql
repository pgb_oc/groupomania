-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le : sam. 01 jan. 2022 à 18:53
-- Version du serveur : 8.0.27
-- Version de PHP : 7.4.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `groupo`
--

-- --------------------------------------------------------

--
-- Structure de la table `bans`
--

DROP TABLE IF EXISTS `bans`;
CREATE TABLE IF NOT EXISTS `bans` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `bannedUserId` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `bans_user_id_banned_user_id` (`userId`,`bannedUserId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `comments`
--

DROP TABLE IF EXISTS `comments`;
CREATE TABLE IF NOT EXISTS `comments` (
  `id` int NOT NULL AUTO_INCREMENT,
  `postId` int DEFAULT NULL,
  `userId` int DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `comments`
--

INSERT INTO `comments` (`id`, `postId`, `userId`, `comment`, `createdAt`, `updatedAt`) VALUES
(1, 1, 1, 'mon premier commentaire', '2022-01-01 16:39:33', '2022-01-01 16:39:33'),
(2, 1, 2, 'commentaire d\'un utilisateur', '2022-01-01 17:04:34', '2022-01-01 17:04:34'),
(3, 2, 2, 'commentaire utilisateur', '2022-01-01 17:05:52', '2022-01-01 17:05:52'),
(4, 2, 2, 'autre commentaire', '2022-01-01 17:12:49', '2022-01-01 17:12:49'),
(5, 3, 2, 'Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...', '2022-01-01 17:15:02', '2022-01-01 17:15:02'),
(6, 0, 0, '', '2022-01-01 17:15:09', '2022-01-01 17:15:09'),
(8, 3, 2, 'autre commentaire', '2022-01-01 17:15:39', '2022-01-01 17:15:39'),
(14, 4, 1, 'test commentaire admin', '2022-01-01 18:04:13', '2022-01-01 18:04:13'),
(16, 3, 2, 'commentaire test', '2022-01-01 18:06:11', '2022-01-01 18:06:11'),
(17, 1, 3, 'commentaire pas bloqué', '2022-01-01 18:07:16', '2022-01-01 18:07:16'),
(19, 3, 2, 'commentaire test', '2022-01-01 18:26:51', '2022-01-01 18:26:51'),
(20, 0, 0, '', '2022-01-01 18:27:31', '2022-01-01 18:27:31'),
(22, 3, 2, 'tes commentaire perso', '2022-01-01 18:31:25', '2022-01-01 18:31:25'),
(23, 1, 3, 'commentaire autre utilisateur', '2022-01-01 18:32:24', '2022-01-01 18:32:24');

-- --------------------------------------------------------

--
-- Structure de la table `likes`
--

DROP TABLE IF EXISTS `likes`;
CREATE TABLE IF NOT EXISTS `likes` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `postId` int DEFAULT NULL,
  `like` int DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `likes`
--

INSERT INTO `likes` (`id`, `userId`, `postId`, `like`, `createdAt`, `updatedAt`) VALUES
(1, 2, 2, 1, '2022-01-01 17:03:46', '2022-01-01 17:03:46'),
(3, 2, 1, 1, '2022-01-01 17:05:04', '2022-01-01 17:05:04'),
(4, 2, 3, 1, '2022-01-01 17:14:40', '2022-01-01 17:14:40'),
(6, 3, 4, 1, '2022-01-01 17:30:16', '2022-01-01 17:30:16'),
(7, 3, 3, 1, '2022-01-01 17:55:27', '2022-01-01 17:55:27');

-- --------------------------------------------------------

--
-- Structure de la table `posts`
--

DROP TABLE IF EXISTS `posts`;
CREATE TABLE IF NOT EXISTS `posts` (
  `id` int NOT NULL AUTO_INCREMENT,
  `userId` int DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `text` text,
  `postUrl` varchar(255) DEFAULT 'default-post.png',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `posts`
--

INSERT INTO `posts` (`id`, `userId`, `title`, `text`, `postUrl`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'Je suis le titre d\'un post', 'Je suis la description d\'un post', 'https://zupimages.net/up/21/52/3md6.png', '2022-01-01 16:35:05', '2022-01-01 16:35:05'),
(2, 2, 'Titre d\'un post', 'description du post', 'https://zupimages.net/up/22/52/hlsc.png', '2022-01-01 17:03:36', '2022-01-01 17:12:39'),
(3, 2, 'Lorem Ipsum is simply dummy text', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.', 'https://zupimages.net/up/22/52/hlsc.png', '2022-01-01 17:14:31', '2022-01-01 17:14:31'),
(4, 3, 'Titre du post de l\'utilisateur indésirable', 'Description du post de l\'indésirable', 'https://zupimages.net/up/21/52/3md6.png', '2022-01-01 17:24:54', '2022-01-01 17:24:54');

-- --------------------------------------------------------

--
-- Structure de la table `sessions`
--

DROP TABLE IF EXISTS `sessions`;
CREATE TABLE IF NOT EXISTS `sessions` (
  `session_id` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `expires` int UNSIGNED NOT NULL,
  `data` mediumtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int NOT NULL AUTO_INCREMENT,
  `firstname` varchar(255) DEFAULT NULL,
  `lastname` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `desc` varchar(255) DEFAULT NULL,
  `imageUrl` varchar(255) DEFAULT 'default-user.png',
  `admin` tinyint(1) DEFAULT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `firstname`, `lastname`, `email`, `password`, `desc`, `imageUrl`, `admin`, `createdAt`, `updatedAt`) VALUES
(1, 'Sarah', 'Connor', 'YWRtaW5AZ3JvdXBvbWFuaWEuY29t', '$2b$10$mAqBVKLHaHpeb6hlxIdMZOpsMalEGrXbKh1.PdOAAk1Ey1h7/UwQS', NULL, 'sarah.jpg1641061800094.jpg', 1, '2022-01-01 16:31:20', '2022-01-01 18:30:00'),
(2, 'Erwin', 'Schrödinger', 'ZXJ3aW5AZ3JvdXBvbWFuaWEuY29t', '$2b$10$R6yqf854RF.Y/YXKUDZHfeGQIkRuFJilMUKzdYX9mvfhZcvae8dme', 'Erwin Rudolf Josef Alexander Schrödinger (en allemand : [ˈɛʁviːn ˈʃʁøːdɪŋɐ]), né le 12 août 1887 à Vienne et mort le 4 janvier 1961 dans la même ville, est un physicien, philosophe et théoricien scientifique autrichien.', 'erwin.jpg1641058852962.jpg', NULL, '2022-01-01 17:02:05', '2022-01-01 17:40:52'),
(3, 'Test mechant', 'vilain', 'YmFuQGdyb3Vwb21hbmlhLmNvbQ==', '$2b$10$8TAlsNLQEeUOuh0CElrRd.Yo02WOvMfu5H7D2BoF50fCqB3nF8ZeC', 'Description du compte test à bannir par un autre user', 'sinok.jpg1641057801158.jpg', NULL, '2022-01-01 17:20:20', '2022-01-01 17:23:21');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
