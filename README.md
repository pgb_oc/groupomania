# PROJET 7 - GROUPOMANIA

## NodeJs - ReactJs - MySQL - React Bootstrap - Css ## 
  
## INSTALLATION ##

You need to install:
<ul>
  <li> Node.js : https://nodejs.org/en/</li>
  <li> MySql :  https://dev.mysql.com/downloads/installer/</li>
</ul>

Clone the repo: 

git@gitlab.com:pgb_oc/groupomania.git

https://gitlab.com/pgb_oc/groupomania.git

## MySQL

In backend/.env config your mysql local access 

Mysql local project default config:
user: root
password: no password

DB_USER=root
DB_PASSWORD=

Mysql Cli :

CREATE DATABASE groupo;
USE groupo;

import groupo.sql file 

## BACKEND

Open terminal:

`cd backend`

`npm install`

`node server`

## FRONTEND

Open another terminal:

`cd frontend`

`npm install`

`npm start`

Navigate your browser to http://localhost:3000/

Use Ctrl+C in the terminal to stop the local server.

Demo Admin access:

Email: admin@groupomania.com
Password: Admin123

User:
Email: erwin@groupomania.com
Password: User1234

User comment banned test:
Email: ban@groupomania.com
Password: User1234


## IMPROVEMENT AFTER DEFENSE (EN)

-  Evaluator recommendation

Add functionality of blocking (ban) a user by another user

- Mentor recommendation

Review of user session management from the MySQL database

- Video URL

https://drive.google.com/file/d/1yryr8pFhWGKlVGXfClFpQHoohep53XOL/view?usp=sharing


## AMELIORATION APRES SOUTENANCE (FR)

- Recommandation de l'évaluateur

Ajout de la fonctionnalité de blocage d'un utilisateur par un autre utilisateur.

- Recommandation du mentor

Revue de la gestion de la session utilisateur à partir de la base de donnée MySQL

- Lien vidéo

https://drive.google.com/file/d/1yryr8pFhWGKlVGXfClFpQHoohep53XOL/view?usp=sharing